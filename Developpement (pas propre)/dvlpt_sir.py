# -*- coding: utf-8 -*-
"""
Created on Wed Apr 21 22:26:01 2021

@author: simon
"""


import numpy as np
import random as rd
import matplotlib.pyplot as plt
from frontieres import frontiere,direction_sir


def chercher_bis(i):
    if ptr[i] < 0 :
        return([i,[0,0]])
    else:
        r = chercher_bis(ptr[i])
        ptr[i] = r[0]
        xdep[i]=xdep[i]+r[1][0]
        ydep[i]=ydep[i]+r[1][1]
    return([ptr[i],[xdep[i],ydep[i]]])

def sim_sir(p,stop):
    c = r*L+r
    nS=N-1
    nI=1
    nR = 0
    etat[c]=1
    I = [c]
    nit = 0
    evolution = np.zeros((stop,3))
    while(nI!=0 and nit<stop):
        U = rd.randint(0,nI-1)
        X = rd.uniform(0,1)
        infecte = I[U]
        if(X>p):
            V = rd.randint(0,K-1)
            vois = voisins[infecte,V]
            if(etat[vois] == 0):
                etat[vois] = 1
                I.append(vois)
                nI=nI+1
                nS=nS-1
        else:
            etat[infecte]=2
            nR = nR + 1
            nI=nI-1
            del I[U]
        evolution[nit]=[nS,nI,nR]
        nit = nit+1
    evolution[nit:stop] = [nS,nI,nR]
    return(evolution,etat)


def sir_wrap(p):
    c = r*L+r #est le 'centre', le premier infécté 
    nI=1
    etat[c]=1
    ptr[c]=-1
    I = [c]
    nit = 0
    xwrap = 0
    ywrap = 0
    res =0
    #res = np.zeros(stop)
    while(nI!=0 and nit<stop and xwrap*ywrap ==0): #and (xwrap*ywrap != 1)(xwrap+ywrap ==0)
        U = rd.randint(0,nI-1)
        X = rd.uniform(0,1)
        infecte = I[U]
        if(X>p):
            V = rd.randint(0,K-1)
            vois = voisins[infecte,V]
            if(etat[vois] == 0):
                ptr[vois]=infecte
                [x,y]=direction_sir(V)
                xdep[vois] = x #on pointe vers le site qui a infecté vois 
                ydep[vois] = y
                etat[vois] = 1 #vois devient infecté
                I.append(vois)
                nI=nI+1
                for j in range(4):
                    s2 = voisins[vois,j]
                    if(j!=V and ptr[s2]!=None): #si j=V on arrive vers infecte, on revient en arriere pas wwrap
                        temp = chercher_bis(vois) #on regarde s'il y a wrap ou pas:
                        dep_x1 = temp[1][0] #ce sont les deplacements entre vois et l'origine
                        dep_y1 = temp[1][1]
                        temp = chercher_bis(s2)
                        #r2 = temp[0] #on sait que la racine est c
                        dep_x2 = temp[1][0] #ce sont les deplacements entre s2 et l'origine
                        dep_y2 = temp[1][1]
                        if(abs(dep_x1-dep_x2)!=0 and abs(dep_x1-dep_x2)!=1):
                            xwrap=1
                        if(abs(dep_y1-dep_y2)!=0 and abs(dep_y1-dep_y2)!=1):
                            ywrap=1
        else:
            etat[infecte]=2
            nI=nI-1
            del I[U]
        nit = nit+1
    if(xwrap == 1 and ywrap ==1):
        res=0
    elif(xwrap == 1 or ywrap==1):
        res=1
    if(nit==stop):
        print("oups")
    return(res) #on renvoie 1 si a un moment donné on a un et un seul wrap a un moment donné, 0 sinon


#on veux savoir s'il y a ou pas un wrap

def wrap_sir_exp(p,r_par):
    global stop,r,L,N,xdep,ydep,ptr,etat,voisins,K
    stop = 1000000
    r = r_par
    L = 2*r+1
    N = L*L
    xdep = [0]*N
    ydep = [0]*N
    ptr =[None]*N
    etat = [0]*N
    V = frontiere(L)
    voisins = V[0]
    K = V[1]
    return(sir_wrap(p))
    """
M=500 #est la taille de l'echantillon
P_min = 0.17
P_max =0.2
N_p = 10 #nombre de probabilités à tester entre P_min et P_max
r = 25 #la taille de la grille est (2*r+1)x(2*r+1)

P = np.linspace(P_min,P_max,N_p)
ech = np.zeros((M,N_p))
for i in range(M):
    for j in range(N_p):
        ech[i,j] = wrap_sir_exp(P[j],r)
    print(100*i/M,"%")
  
res = np.sum(ech,axis=0)/M

plt.plot(P,res,'bo')
#plt.axvline(x=0.1765,color='red',alpha=0.5,label="reference")
plt.xlabel("$c$")
plt.ylabel("$R^{(1)}$")
plt.legend()
est = P[np.argmax(res)]

indice = np.argmax(res)
q=1.96
borne_inf = res[indice]-q*np.sqrt(res[indice]*(1-res[indice])/M)
plt.hlines(borne_inf,P[0],P[len(P)-1],linestyles=':')
plt.legend(['simulation','seuil'])
IC = P[res>borne_inf]
print(IC)
print("p_c est estimé par "+str(est))"""


p = 0.1
stop = 1000000
r = 2
L = 2*r+1
N = L*L
xdep = [0]*N
ydep = [0]*N
ptr =[None]*N
etat = [0]*N
V = frontiere(L)
voisins = V[0]
K = V[1]

c = r*L+r #est le 'centre', le premier infécté 
nI=1
etat[c]=1
ptr[c]=-1
I = [c]
nit = 0
xwrap = 0
ywrap = 0
res =0
#res = np.zeros(stop)
while(nI!=0 and nit<stop and xwrap*ywrap ==0): #and (xwrap*ywrap != 1)(xwrap+ywrap ==0)
    U = rd.randint(0,nI-1)
    X = rd.uniform(0,1)
    infecte = I[U]
    if(X>p):
        V = rd.randint(0,K-1)
        vois = voisins[infecte,V]
        if(etat[vois] == 0):
            ptr[vois]=infecte
            [x,y]=direction_sir(V)
            xdep[vois] = x #on pointe vers le site qui a infecté vois 
            ydep[vois] = y
            etat[vois] = 1 #vois devient infecté
            I.append(vois)
            nI=nI+1
            
            for j in range(4):
                s2 = voisins[vois,j]
                if(ptr[s2]!=None): #si j=V on arrive vers infecte, on revient en arriere pas wwrap
                    temp = chercher_bis(vois) #on regarde s'il y a wrap ou pas:
                    dep_x1 = temp[1][0] #ce sont les deplacements entre vois et l'origine
                    dep_y1 = temp[1][1]
                    temp = chercher_bis(s2)
                    #r2 = temp[0] #on sait que la racine est c
                    dep_x2 = temp[1][0] #ce sont les deplacements entre s2 et l'origine
                    dep_y2 = temp[1][1]
                    if((abs(dep_x1-dep_x2)!=0) and (abs(dep_x1-dep_x2)!=1)):
                        xwrap=1
                    if((abs(dep_y1-dep_y2)!=0) and (abs(dep_y1-dep_y2)!=1)):
                        ywrap=1
    else:
        etat[infecte]=2
        nI=nI-1
        del I[U]
    nit = nit+1
    print(np.array(ptr).reshape((L,L)),'\n')
    print('xwrap',xwrap)
    print('ywrap',ywrap)
    #print('deplacements en x:','\n', np.array(xdep).reshape((L,L)),'\n')
    #print('deplacements en y:', '\n',np.array(ydep).reshape((L,L)),'\n')

if(xwrap == 1 and ywrap ==1):
    res=0
elif(xwrap == 1 or ywrap==1):
    res=1
if(nit==stop):
    print("oups")
