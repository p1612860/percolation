# -*- coding: utf-8 -*-
"""
Created on Thu Apr 22 12:17:34 2021

@author: simon
"""

#on utilise ce fichier uniquement pour faire des figures, il n'est pas 'propre'.

from frontieres import frontiere_v2,frontiere_triangle,frontiere_hexagone,frontiere_triangle_per,direction_triangle
from wrap_v3 import div_eucl
import numpy as np
import matplotlib.pyplot as plt
import random as rd

def montrer_graphe(sites=1,aretes=1,nombre = 0):
    fig, ax = plt.subplots(1)
    for i in range(N):
        [y,x] = div_eucl(i,L)
        for j in range(K):
            if(aretes and voisins[i,j]!=i):
                [y2,x2]=div_eucl(voisins[i,j],L)
                plt.plot([x,x2],[y,y2],c='blue',zorder=1)
        if(sites):
            if(nombre==0):
                ax.scatter(x,y,c='red',s=100,alpha=1,zorder=2)
            else:
                plt.text(x,y,str(i),c='red',fontsize=16,zorder=2)

def montrer_graphe_periodique(sites=1,aretes=1):
    plt.ylim([0,L])
    plt.xlim([0,L])
    fig, ax = plt.subplots(1)
    for i in range(N):
        [y,x] = div_eucl(i,L)
        for j in range(K):
            if(aretes and voisins[i,j]!=i):
                [y2,x2]=div_eucl(voisins[i,j],L)
                plt.plot([x,x2],[y,y2],c='blue',zorder=1)
        #plt.text(x,y,str(i),c='red',fontsize=16,zorder=2)
        if(sites):
            ax.scatter(x,y,c='red',s=100,alpha=1,zorder=2)



#L = 10
L=5
N = L*L
#print(frontiere(L))
#ptr =[None]*N

#V = frontiere_v2(L)
V = frontiere_triangle(L)
#V = frontiere_hexagone(L)

voisins = V[0]
K = V[1]

V = frontiere_triangle_per(L)
voisins_per = V[0]


plt.ylim([-1,L+1])
plt.xlim([-1,L+1])
fig, ax = plt.subplots(1)

X = np.arange(7)
Y = np.arange(4)

"""
plt.plot([0,4],[0,0])
plt.plot([0,4],[2,2])
plt.plot([0,4],[4,4])
plt.plot([1,])

"""

montrer_graphe(1,1,1)

for i in range(N):
    [y,x] = div_eucl(i,L)
    if(i%L == 0): #bord gauche
        plt.plot([x,x-1],[y,y],alpha=0.5,c='blue',zorder=1)
        plt.plot([x,x-1],[y,y-1],alpha=0.5,c='blue',zorder=1)
        plt.text(x-1,y,str(voisins_per[i,1]),c='red',fontsize=16,zorder=2)
    if((i+1)%L == 0): #bord droit
        plt.plot([x,x+1],[y,y],alpha=0.5,c='blue',zorder=1)
        plt.plot([x,x+1],[y,y+1],alpha=0.5,c='blue',zorder=1)
        plt.text(x+1,y,str(voisins_per[i,0]),c='red',fontsize=16,zorder=2)
    if(i<L):#en bas
        plt.plot([x,x],[y,y-1],alpha=0.5,c='blue',zorder=1)
        plt.plot([x,x+1],[y-1,y],alpha=0.5,c='blue',zorder=1)
        plt.text(x,y-1,str(voisins_per[i,2]),c='red',fontsize=16,zorder=2)
    if(i>=L*(L-1)): #en haut
        plt.plot([x,x],[y,y+1],alpha=0.5,c='blue',zorder=1)
        plt.plot([x-1,x],[y,y+1],alpha=0.5,c='blue',zorder=1)
        plt.text(x,y+1,str(voisins_per[i,3]),c='red',fontsize=16,zorder=2)

plt.text(5,5,'0',c='red',fontsize=16,zorder=2)
plt.text(-1,-1,'24',c='red',fontsize=16,zorder=2)

    
        



def percoler_simple_sites(p):
    etat=[0]*N #il y a N sites
    for i in range(N):
        U = rd.uniform(0,1)
        if(U<p):
            etat[i]=1
    return(etat)


def frontiere_carre_cov_per(L):#c'est le graphe correspondant du graphe carré bord periodiques
    N=L*L
    K=6
    fr = np.zeros([N,K]).astype('int')
    for i in range(0,N):
        fr[i,0] = (i + 1)%N #droite
        fr[i,1] = (i - 1)%N #gauche
        fr[i,2] = (i - L)%N #haut
        fr[i,3] = (i + L)%N #bas
        if(i%L == 0):#bord gauche
            #fr[i,1]=i
            fr[i,1] = i + L -1 
            #fr[i,7] = (i -1)%N
            #fr[i,6] = (i +2*L-1)%N
        if((i+1)%L ==0):#bord droit
            #fr[i,0]=i
            fr[i,0] = i - L +1 
        if(i >= (L*(L-1))):#bord d'en bas
            #fr[i,3]=i    
            fr[i,3] = i + L - L*L
        if (i<L): #bord d'en haut
            #fr[i,2]=i
            fr[i,2]= i + L*(L-1)
    for i in range(N):
        fr[i,4] = fr[fr[i,i%2],3] #bas droite ou gauche
        fr[i,5] = fr[fr[i,(1+i)%2],2] #haut gauche ou droite
    return(fr,K)

def frontiere_carre_cov(L): #marche pas trop bien
    N=L*L
    K=6
    fr = np.zeros([N,K]).astype('int')
    for i in range(0,N):
        fr[i,0] = (i + 1)%N #droite
        fr[i,1] = (i - 1)%N #gauche
        fr[i,2] = (i - L)%N #haut
        fr[i,3] = (i + L)%N #bas
        if(i%L == 0):#bord gauche
            fr[i,1] = i 
        if((i+1)%L ==0):#bord droit
            fr[i,0] = i
        if(i >= (L*(L-1))):#bord d'en bas
            fr[i,3] = i
        if (i<L): #bord d'en haut
            fr[i,2]= i
    for i in range(N):
        fr[i,4] = fr[fr[i,i%2],3] #bas droite ou gauche
        fr[i,5] = fr[fr[i,(1+i)%2],2] #haut gauche ou droite
    return(fr,K)




"""
for i in range(L):
    voisins[i*L,5-i%2]=i*L
    voisins[i*L-1,4+i%2]=i*L-1
voisins[24,4]=24


"""

"""
V = frontiere_carre_cov_per(5)    
voisins =V[0]
print(voisins)



K = V[1]
"""
#montrer_graphe()
"""  
etat =percoler_simple_sites(0.6)
for i in range(N):
    if(etat[i]==1):
        [y,x] = div_eucl(i,L)
        ax.scatter(x,y,c='red',s=10,alpha=1,zorder=2)

"""
"""
xpair = [0.5,1.5,2.5]
ximpair =[0,1,2,3]
ypair = [0,1,2,3]
yimpair = [0.5,1.5,2.5]


for i in range(3):
    for j in range(4):
        ax.scatter(xpair[i],ypair[j],c='blue',zorder=2)
        ax.scatter(ximpair[j],yimpair[i],c='blue',zorder=2)

for i in range(3):
    for j in range(3):
        plt.plot([xpair[j],ximpair[j]],[ypair[i],yimpair[i]],c='violet',zorder=1)
        plt.plot([xpair[j],ximpair[j+1]],[ypair[i],yimpair[i]],c='violet',zorder=1)
        plt.plot([ximpair[j],xpair[j]],[yimpair[i],ypair[i+1]],c='violet',zorder=1)
        plt.plot([ximpair[j+1],xpair[j]],[yimpair[i],ypair[i+1]],c='violet',zorder=1)

plt.plot([0.5,2.5],[1,1],c='violet',zorder=1)
plt.plot([0.5,2.5],[2,2],c='violet',zorder=1)
plt.plot([1,1],[0.5,2.5],c='violet',zorder=1)
plt.plot([2,2],[0.5,2.5],c='violet',zorder=1)



"""



plt.axis('off')
plt.show()














