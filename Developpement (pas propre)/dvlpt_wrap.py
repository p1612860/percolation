# -*- coding: utf-8 -*-
"""
Created on Tue Apr 20 13:53:12 2021

@author: simon
"""
import numpy as np
import random as rd
import matplotlib.pyplot as plt
from frontieres import frontiere,direction_carre_cov_per
from frontieres import frontiere_triangle_per,direction_triangle
from wrap_v3 import P_wrap
import scipy 

def chercher(i):
    if ptr[i] < 0 :
        return i
    else:
        ptr[i] = chercher(ptr[i])
    return ptr[i]

def chercher_bis(i):
    if ptr[i] < 0 :
        return([i,[0,0]])
    else:
        r = chercher_bis(ptr[i])
        ptr[i] = r[0]
        xdep[i]=xdep[i]+r[1][0]
        ydep[i]=ydep[i]+r[1][1]
    return([ptr[i],[xdep[i],ydep[i]]])

def GrilleToArbre(test,L):
    global N,ptr,xdep,ydep,voisins,K
    N= L*L
    ptr = [None]*N
    xdep = [None]*N
    ydep = [None]*N
    V =frontiere(L)
    voisins = V[0]
    K = V[1]
    n = len(test)
    for i in range(n):
        indice = test[i][0]+test[i][1]*L
        ptr[indice] = -1
        xdep[indice] = 0
        ydep[indice]=0  
    for i in range(N):
        if(ptr[i]!=None):
            chercher_bis(i)

def permutation():
    ordre = np.arange(N)
    for i in range(0,N):
        j= rd.randint(0,N-1)
        temp = ordre[i]
        ordre[i] = ordre[j]
        ordre[j] = temp 
    return(ordre)

def div_eucl(n,q):
    return(n//q,n%q)
""
def points():
    plt.ylim([0,L])
    plt.xlim([0,L])
    fig, ax = plt.subplots(1)
    for i in range(N):
        if(ptr[i]!=None):
            [y,x] = div_eucl(i,L)
            ax.scatter(x,y,c='red')
    plt.show()


def direction_hexagone(j,i,L=0):
    d = (i%L)%4
    x=0
    y=0
    if(j==0):
        x=1
    if(j==1):
        x=-1
    if(j==2):
        if(d==0):
            x=1
            y=-1
        elif(d==1):
            x=-1
            y=1
        elif(d==2):
            x=1
            y=1
        elif(d==3):
            x=-1
            y=-1
    return([x,y])
    

            


"""
ptr = [None,
 8,
 None,
 8,
 8,
 None,
 None,
 None,
 -15,
 8,
 None,
 8,
 8,
 8,
 None,
 None,
 None,
 22,
 None,
 8,
 8,
 8,
 8,
 None,
 8]
frontiere(5)
"""
#GrilleToArbre(test,L)
#points()

#ordre = np.array([15, 16, 23, 12,  7,  8, 26,  9, 20, 10, 22, 34, 18, 24,  1, 30, 35,25, 27, 33, 11, 29, 19,  6,  5, 14, 28, 17,  2, 32,  3, 13, 31,  4, 0, 21])

"""
ordre = np.array([ 1, 21, 27, 19,  2,  9,  8, 15, 24, 26, 14, 34, 31, 17, 20,  3,  4,
       35,  5, 32, 28, 13, 22, 10, 30,  0, 23,  7, 33, 18,  6, 11, 16, 25,
       29, 12])
"""

def frontiere_hexagone_per(L): #ºATTENTION L DOIT ETRE MULTIPLE DE 4!!!!!!(sinon il n'y a pas de grille perdiodique et l'algo plante)
    K = 3
    N   = L*L
    voisins = np.zeros((N,3)).astype('int')
    truc = [L,-L,-L,L]
    chose = [1,-1,1,-1]

    for i in range(N):
        voisins[i,0]= (i+1)%N #droite
        voisins[i,1] = (i-1)%N #gauche
        voisins[i,2] = (i +truc[i%4]+chose[i%4])%N#diagonale
        if(i%L == 0 ):#bord gauche
            voisins[i,1]=i+L-1
        if((i+1)%L == 0):#bord droit
            voisins[i,0]=i-L+1
            #voisins[i,2] = i+1
    return(voisins,K)
"""
        if(i%L == 0 ):#bord gauche
            voisins[i,1]=i+L-1
        if((i+1)%L == 0):#bord droit
            voisins[i,0]=i-L+1
            voisins[i,2] = i+1
        if(i<L):#bord haut
            if(cosa_arriba[i%4]==1):
                voisins[i,2]=(i-L+chose[i%4])%N
        if(i+L >=N):#bord bas
            if(cosa_abajo[(i%L)%4]==1):
                voisins[i,2]=(i+L+chose[(i%L)%4])%N
"""


"""
L = 4*5
K = 3
N = L*L
ptr = [None] * N
ordre = permutation()

#ordre = [ 3, 11,  9,  7, 15,  8, 13,  0,  2, 12,  5,  6,  1, 14, 10,  4]
V = frontiere_hexagone_per(L)
voisins = V[0]
"""
"""
print(voisins)
for i in range(N):
    print(sum(voisins==i))
    """
    
"""
xdep = [None] * N
ydep = [None] * N
#V = percoler()
direction = lambda j,i: direction_hexagone(j,i,L)

wrap =0
res = np.zeros([N]).astype('int')
for i  in range(0,N):
    r1 = ordre[i]
    s1 = r1
    ptr[s1] = -1
    xdep[s1]=0
    ydep[s1]=0
    for j in range(0,K):
        s2 = voisins[s1,j]#si j=0 on va à droite, j=1 gauche; j=2 haut;j=3 bas
        if(ptr[s2]!=None and s2!=j):
            temp = chercher_bis(s2)
            r2 = temp[0] #est la racine de reseau du voisin
            dep_x2 = temp[1][0] #ce sont les deplacements de s2 vers sa racine
            dep_y2 = temp[1][1]
            if(r2 != r1):
                [x,y] = direction(j,s1)#c'est le deplacement entre s1 et s2
                temp = chercher_bis(s1)
                dep_x1 = temp[1][0] #ce sont les deplacements de s1 vers sa racine
                dep_y1 = temp[1][1]
                if (ptr[r2]<ptr[r1]): #le reseau voisin est plus grand, on fusionne le reseau de r1 dans r2
                    ptr[r2]=ptr[r2]+ptr[r1]
                    ptr[r1]=r2
                    xdep[r1]= x+dep_x2-dep_x1
                    ydep[r1]=y+dep_y2-dep_y1
                    r1=r2
                else: #le reseau voisin est plus petit, on le fusionne dans celui de r1
                    ptr[r1] = ptr[r1]+ptr[r2]
                    ptr[r2] = r1
                    xdep[r2]= -x-dep_x2+dep_x1
                    ydep[r2]= -y - dep_y2+dep_y1
            elif(wrap ==0):
                temp = chercher_bis(s1)
                dep_x1 = temp[1][0] #ce sont les deplacements de s1 vers sa racine
                dep_y1 = temp[1][1]
                #dep_x1 = xdep[s1] #ce sont les deplacements de s1
                #dep_y1 = ydep[s1]
                if(abs(dep_x1-dep_x2) + abs(dep_y1-dep_y2)!=1):
                    print('wrap!!!!')
                    print(i)
                    points()
                    #ptr[s1] =None
                    points()
                    wrap =1
                    #print("deplacement en x",np.array(xdep).reshape((L,L)),'\n')
                    #print("deplacement en y",np.array(ydep).reshape((L,L)),'\n')
                    #print(np.array(ptr).reshape((L,L)))
    #print(np.array(ptr).reshape((L,L)),'\n')
"""
"""
i=11
r1 = ordre[i]
s1 = r1
ptr[s1] = -1
xdep[s1]=0
ydep[s1]=0

j=0

    s2 = voisins[s1,j]#si j=0 on va à droite, j=1 gauche; j=2 haut;j=3 bas
    if(ptr[s2]!=None and s2!=j):
        temp = chercher_bis(s2)
        r2 = temp[0] #est la racine de reseau du voisin
        dep_x2 = temp[1][0] #ce sont les deplacements de s2 vers sa racine
        dep_y2 = temp[1][1]
        if(r2 != r1):
            [x,y] = direction(j,s1)#c'est le deplacement entre s1 et s2
            temp = chercher_bis(s1)
            dep_x1 = temp[1][0] #ce sont les deplacements de s1 vers sa racine
            dep_y1 = temp[1][1]
            if (ptr[r2]<ptr[r1]): #le reseau voisin est plus grand, on fusionne le reseau de r1 dans r2
                ptr[r2]=ptr[r2]+ptr[r1]
                ptr[r1]=r2
                xdep[r1]= x+dep_x2-dep_x1
                ydep[r1]=y+dep_y2-dep_y1
                r1=r2
            else: #le reseau voisin est plus petit, on le fusionne dans celui de r1
                ptr[r1] = ptr[r1]+ptr[r2]
                ptr[r2] = r1
                xdep[r2]= -x-dep_x2+dep_x1
                ydep[r2]= -y - dep_y2+dep_y1
        elif(wrap ==0):
            temp = chercher_bis(s1)
            dep_x1 = temp[1][0] #ce sont les deplacements de s1 vers sa racine
            dep_y1 = temp[1][1]
            #dep_x1 = xdep[s1] #ce sont les deplacements de s1
            #dep_y1 = ydep[s1]
            if(abs(dep_x1-dep_x2) + abs(dep_y1-dep_y2)!=1):
                print('wrap!!!!')
                print(i)
                points()
                ptr[s1] =None
                points()
                wrap =1
                #print("deplacement en x",np.array(xdep).reshape((L,L)),'\n')
                #print("deplacement en y",np.array(ydep).reshape((L,L)),'\n')
                #print(np.array(ptr).reshape((L,L)))

print(np.array(ptr).reshape((L,L)))

j=1

s2 = voisins[s1,j]#si j=0 on va à droite, j=1 gauche; j=2 haut;j=3 bas
if(ptr[s2]!=None and s2!=j):
    temp = chercher_bis(s2)
    r2 = temp[0] #est la racine de reseau du voisin
    dep_x2 = temp[1][0] #ce sont les deplacements de s2 vers sa racine
    dep_y2 = temp[1][1]
    if(r2 != r1):
        [x,y] = direction(j,s1)#c'est le deplacement entre s1 et s2
        temp = chercher_bis(s1)
        dep_x1 = temp[1][0] #ce sont les deplacements de s1 vers sa racine
        dep_y1 = temp[1][1]
        if (ptr[r2]<ptr[r1]): #le reseau voisin est plus grand, on fusionne le reseau de r1 dans r2
            ptr[r2]=ptr[r2]+ptr[r1]
            ptr[r1]=r2
            xdep[r1]= x+dep_x2-dep_x1
            ydep[r1]=y+dep_y2-dep_y1
            r1=r2
        else: #le reseau voisin est plus petit, on le fusionne dans celui de r1
            ptr[r1] = ptr[r1]+ptr[r2]
            ptr[r2] = r1
            xdep[r2]= -x-dep_x2+dep_x1
            ydep[r2]= -y - dep_y2+dep_y1
    elif(wrap ==0):
        temp = chercher_bis(s1)
        dep_x1 = temp[1][0] #ce sont les deplacements de s1 vers sa racine
        dep_y1 = temp[1][1]
        #dep_x1 = xdep[s1] #ce sont les deplacements de s1
        #dep_y1 = ydep[s1]
        if(abs(dep_x1-dep_x2) + abs(dep_y1-dep_y2)!=1):
            print('wrap!!!!')
            print(i)
            points()
            ptr[s1] =None
            points()
            wrap =1
            #print("deplacement en x",np.array(xdep).reshape((L,L)),'\n')
            #print("deplacement en y",np.array(ydep).reshape((L,L)),'\n')
            #print(np.array(ptr).reshape((L,L)))

print(np.array(ptr).reshape((L,L)))


j=2

s2 = voisins[s1,j]#si j=0 on va à droite, j=1 gauche; j=2 haut;j=3 bas
if(ptr[s2]!=None and s2!=j):
    temp = chercher_bis(s2)
    r2 = temp[0] #est la racine de reseau du voisin
    dep_x2 = temp[1][0] #ce sont les deplacements de s2 vers sa racine
    dep_y2 = temp[1][1]
    if(r2 != r1):
        [x,y] = direction(j,s1)#c'est le deplacement entre s1 et s2
        temp = chercher_bis(s1)
        dep_x1 = temp[1][0] #ce sont les deplacements de s1 vers sa racine
        dep_y1 = temp[1][1]
        if (ptr[r2]<ptr[r1]): #le reseau voisin est plus grand, on fusionne le reseau de r1 dans r2
            ptr[r2]=ptr[r2]+ptr[r1]
            ptr[r1]=r2
            xdep[r1]= x+dep_x2-dep_x1
            ydep[r1]=y+dep_y2-dep_y1
            r1=r2
        else: #le reseau voisin est plus petit, on le fusionne dans celui de r1
            ptr[r1] = ptr[r1]+ptr[r2]
            ptr[r2] = r1
            xdep[r2]= -x-dep_x2+dep_x1
            ydep[r2]= -y - dep_y2+dep_y1
    elif(wrap ==0):
        temp = chercher_bis(s1)
        dep_x1 = temp[1][0] #ce sont les deplacements de s1 vers sa racine
        dep_y1 = temp[1][1]
        #dep_x1 = xdep[s1] #ce sont les deplacements de s1
        #dep_y1 = ydep[s1]
        if(abs(dep_x1-dep_x2) + abs(dep_y1-dep_y2)!=1):
            print('wrap!!!!')
            print(i)
            points()
            ptr[s1] =None
            points()
            wrap =1
            #print("deplacement en x",np.array(xdep).reshape((L,L)),'\n')
            #print("deplacement en y",np.array(ydep).reshape((L,L)),'\n')
            #print(np.array(ptr).reshape((L,L)))
"""
print(np.array(ptr).reshape((L,L)))

"""
test = np.array([[18, None, 3, -3, None, None],
       [18, 18, 18, None, -1, None],
       [None, 18, 18, None, None, 18],
       [-15, 18, None, 18, 18, 18],
       [18, None, None, None, 18, None],
       [None, None, 3, None, None, -1]], dtype=object)

ptr = test.reshape(N).tolist()

r1 = 15
s1 = r1
ptr[s1] = -1
xdep[s1]=0
ydep[s1]=0

s2 = 14

"""
"""
j=-1
if(ptr[s2]!=None ):
    r2 =chercher(s2)
    if(r2 != r1):
        if (ptr[r2]<ptr[r1]):
            ptr[r2]=ptr[r2]+ptr[r1]
            ptr[r1]=r2
            direction(j,r1)
            r1=r2
        else:
            ptr[r1] = ptr[r1]+ptr[r2]
            ptr[r2] = r1
    if(r2 == r1):
        print('yes')
        [x1,y1]=div_eucl(r1,L)
        [x1,y1]=div_eucl(r2,L)
      """  

"""
L=4
N=L*L
test_r = np.array([[1,2,3,-4],[None,None,None,None],[None,None,None,None],[None,None,None,None]], dtype=object)


xdep = [None] * N
ydep = [None] * N

xdep[0:4] = [1,1,1,0]

ydep[0:4] = [0,0,0,0]

ptr = test_r.reshape(N).tolist()
"""


"""
res = np.zeros([N]).astype('int')
for i  in range(0,N):
    r1 = ordre[i]
    s1 = r1
    ptr[s1] = -1
    xdep[s1]=0
    ydep[s1]=0
    for j in range(0,K):
        s2 = voisins[s1,j]#si j=1 on va à droite, j=2 gauche; j=3 haut;j=4 bas
        if(ptr[s2]!=None and s2!=j):
            temp = chercher_bis(s2)
            r2 = temp[0]
            dep_x2 = temp[1][0] #ce sont les deplacements de s2
            dep_y2 = temp[1][1]
            if(r2 != r1):
                if (ptr[r2]<ptr[r1]):
                    ptr[r2]=ptr[r2]+ptr[r1]
                    ptr[r1]=r2
                    direction(j,r1)
                    r1=r2
                else:
                    ptr[r1] = ptr[r1]+ptr[r2]
                    ptr[r2] = r1
                    direction(j,r2)
            if(r2 == r1):
                dep_x1 = xdep[s1] #ce sont les deplacements de s1
                dep_y1 = ydep[s1]
                if(abs(dep_x1-dep_x2) + abs(dep_y1-dep_y2)!=1):
                    print('wrap!!!!')
                    j=K
                    i=N

"""