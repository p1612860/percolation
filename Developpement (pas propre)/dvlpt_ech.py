import numpy as np
import random as rd
import matplotlib.pyplot as plt
from wrap_v3 import P_wrap
from scipy.stats import binom
import scipy
import time
from echantillonage import conv_bin
import statistics



def g(x):
    return(2*np.arcsin(np.sqrt(x)))


def inv_g(y):
    return(np.sin(y/2)*np.sin(y/2))

print(g(inv_g(0.5)))

print(inv_g(g(0.75)))

L = 50
N = L*L
M=200 #est la taille de l'échantillon
R_1_ech = np.zeros((M,N))    
R_2_ech = np.zeros((M,N)) 
#R_conv = np.zeros([N])  
 


#ATTENTION, pour la grille hexagonale, L doit être un multiple de 4 (sinon il n'y a pas de graphe periodique)
for i in range(M):
    R_1_ech[i,:] = P_wrap(L,1,"carre")[0]
    print(i/M * 100)

R1 = np.mean(R_1_ech,axis=0)
P = np.arange(N)/(N-1)

S1 = conv_bin(R1)
#plt.plot(P,R1)
plt.plot(P,S1,c='blue')
est = P[np.argmax(S1)]

q = 1.96


borne_inf_v2 = R1-q*statistics.stdev(S1)/np.sqrt(M)
inf_test = conv_bin(borne_inf_v2)
m = inf_test[np.argmax(S1)]
print(P[S1>m])

"""
borne_inf = inv_g(g(R1)-q/np.sqrt(M))
"""

"""
plt.hlines(y=inf_test[np.argmax(R1)],xmin=0,xmax=1)
plt.plot(P,inf_test,c='red')
I = np.zeros(N)
m = inf_test[np.argmax(R1)]
for i in range(N):
    if(S1[i]>m):
        I[i]=1
    
print(P[I==1])
"""




