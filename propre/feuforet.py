# -*- coding: utf-8 -*-

from turtle import *
from math import *
from random import *

##### GRAPHIQUE######
def couleur(X,Y):
    if FORET[X][Y]=='A':
        color('green')
    elif FORET[X][Y]=='C':
        color('grey')
    elif FORET[X][Y]=='F':
        color('red')
    elif FORET[X][Y]=='V':
        color('white')

def carre(a):
    begin_fill()
    for i in range(4):
        forward(a)
        left(90)
    end_fill()
    forward(a)

def cellule(X,Y):
    up()
    x,y=A*X,A*Y
    goto(x,y)
    down()
    couleur(X,Y)
    dot(A)
    #carre(A)

def affiche(foret):
    for Y in range(len(foret)):
        for X in range(len(foret[Y])):
            cellule(Y,X)

####### MODELISATION ####

def suivant(foret,p):
    s=[['V' for i in range(len(foret))] for j in range(len(foret))]
    for Y in range(1,len(foret)-1):
        for X in range(1,len(foret[Y])-1):

            if foret[Y][X]=='F':
                s[Y][X]='C'

            elif foret[Y][X]=='A':
                if foret[Y-1][X]=='F' or foret[Y+1][X]=='F' or foret[Y][X-1]=='F' or foret[Y][X+1]=='F':
                    if random()< p:
                        s[Y][X]='F' # le voisin prend feu
                    else:
                        s[Y][X]='A'
                else:
                    s[Y][X]='A'


            elif foret[Y][X]=='C':
                s[Y][X]='C'

            elif foret[Y][X]=='V':
                s[Y][X]='V'



    return s


def feu(foret):
    for Y in range(1,len(foret)-1):
        for X in range(1,len(foret[Y])-1):
            if foret[Y][X]=='F':
                return True

    return False


### CALCULS ###
def degats(foret):
    c=0 #nombre d'arbres brulés
    for Y in range(1,len(foret)-1):
        for X in range(1,len(foret[Y])-1):
            if foret[Y][X]=='C':
                c=c+1
    return c


###CONSTANTES
#A=Arbre C=Cendre F=Feu V=Vide

N=10 # taille de la foret
FORET=[['V' for i in range(N+1+1)]]+[['V']+[choice(['A']) for i in range(N)]+['V'] for j in range(N)]+[['V' for i in range(N+1+1)]]  #Structure de la foret

FORET[1][randint(1,N)]='F' # Foyer
P=0.6 # ProbabilitÃ© de propagation Ã  un voisin

A=15 # Taille de cellule


##### INIT ####
def init():
    hideturtle()
    tracer(120)
    up()



#### FEU ###

init()
affiche(FORET)
while feu(FORET):
    FORET=suivant(FORET,P)
    affiche(FORET)
