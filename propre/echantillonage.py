import numpy as np
import random as rd
import matplotlib.pyplot as plt
from wrap_v3 import P_wrap #cette fonction est dans le fichier wrap_v3, il faut avoir ce fichier pour faire de l'echantillonage
from scipy.stats import binom
import scipy
import time
import statistics

temps = time.time()

def conv_bin(X):
    N = len(X)
    S = np.zeros(N)
    P = np.arange(N)/(N-1) 
    for n in range(N):
        S = S + binom.pmf(n,N,P)*X[n]
    return(S)

L = 100 #La grille a L*L sites
N = L*L #N est le nombre de sites
M=1000 #est la taille de l'échantillon
#pour changer le graphe il faut changer le troisieme argument de P_wrap (lire les parametres)

#========================================================================================================================================================================================================================================================================================
#PARAMETRES DE P_wrap:                                                                                                                                                                                                                                                                  #
#ATTENTION, pour la grille hexagonale, L doit être un multiple de 4, sinon il n'y a pas de graphe periodique et notre fonction marche pas                                                                                                                                               #
#Pour faire des simulations de l'evenement 'il y a wrap' il suffit d'appeler la fonction P_wrap.                                                                                                                                                                                        #
#Le premier argument de P_wrap est la longueur de la grille, le deuxieme est le p maximal à estimer (non implenenté), le troisième le graphe avec lequel on travaille                                                                                                                   #
#en troisieme option de P_wrap on peut mettre "hexagone" pour la grille 'honeycomb', carre pour la percolation de sites en L^2, "carre_covering" pour la percolation d'aretes en L^2, "triangle" pour le réseau triangulaire, carre_match pour la grille correspdante (non implementée) #
#=======================================================================================================================================================================================================================================================================================#
R_1_ech = np.zeros((M,N))    

for i in range(M):
    R_1_ech[i,:] = P_wrap(L,1,"hexagone")[0]
    print(round(i/M * 100,3),'%')

R1 = np.mean(R_1_ech,axis=0)
P = np.arange(N)/(N-1)
print("convolution...")
S1 = conv_bin(R1)
plt.plot(P,S1,c='red')
est = P[np.argmax(R1)]

plt.xlabel('$p$')
plt.ylabel('$\hat{R}^{(1)}$')
print("p_c est estimé par "+str(est))
q=scipy.stats.norm.ppf(0.975)

borne_inf = R1-q*statistics.stdev(S1)/np.sqrt(M) 
inf_test = conv_bin(borne_inf)#ceci est la borne inferieure de l'intervalle de confiance pour R^1(\hat(p))
m = inf_test[np.argmax(S1)]
IC = P[S1>m]
plt.title('Grille hexagonale: p_c$\in$]'+str(round(IC[0],5))+','+str(round(IC[len(IC)-1],5))+'[')
plt.hlines(m,0,1,linestyle=':')
plt.legend(['Estimation','Seuil'])

print("ca a pris ",time.time()-temps," secondes")

