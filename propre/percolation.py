import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import copy
import time
#from networkx.algorithms import approximation as app
    
def percoler(G,p,bound=0,out=False) :
    H=copy.deepcopy(G)
    tabE = list(G.edges)
    nedge = G.number_of_edges()
    
    
    for i in range(0,nedge) :
        if(not bound) :
            if(not np.random.binomial(1,p)) : 
                H.remove_edge(tabE[i][0],tabE[i][1])
     #   else : 
            
            
    return H

def meanElem (L) : 
    n = len(L)
    res = np.repeat(0,n)
    
    for i in range(0,n) : 
        res[i] = len(L[i])
        
    return np.mean(res)     
        
beg = time.time()
  
M = 30 #X
N = 30 #Y
L = 1 #Z
n = 20 #nombre de proba
p = np.linspace(0.1,0.8,n)
disp = True
res = np.repeat(0.0,n)
res2 = np.repeat(0.0,n)

#G = nx.grid_graph([M,N])
G = nx.grid_2d_graph(M,N)
#G = nx.triangular_lattice_graph(M,N)
#G = nx.hexagonal_lattice_graph(M,N)
pos = nx.spectral_layout(G)
nbnode = G.number_of_nodes()

#bound = list([tuple([0,0]),tuple([0,5]),tuple([5,0]),tuple([5,5])])
#nx.bidirectional_shortest_path(G,bound[0],bound[1])

for i in range(0,n) :     
    
    #if(i == 5) : p[i] = 0.4
    H = percoler(G,p[i])
    J = max(nx.connected_components(H),key=len)
    #if(i == 5) : print(meanElem(list(nx.connected_components(H))),"######")
     
    if(disp) :
        JJ = G.subgraph(J)
        nx.draw(H,pos,node_size=10,with_labels=False)
        nx.draw(JJ,pos,node_color='red',node_size=10)
        print("*****")
        print("Nb total noeud : ", nbnode)
        print("Taille composante connexe : ", len(J))
        print("i = ", i, " p = ", p[i])
        plt.title("p = "+str(p[i]))
        plt.show()
        time.sleep(0.5)
        
    res[i] = len(J)/nbnode

Tps = time.time() - beg

print("Time execution :", Tps)

input("Enter key ~")
plt.plot(p,res)
plt.show()


