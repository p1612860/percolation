import numpy as np
import random as rd
from frontieres import frontiere_carre_match,frontiere,direction_carre,direction_carre_match
from frontieres import frontiere_carre_cov_per,direction_carre_cov_per
from frontieres import frontiere_triangle_per,direction_triangle
from frontieres import frontiere_hexagone_per,direction_hexagone

######################################################################################################################################################################
#On fait appel a des fonctions du fichier frontieres                                                                                                                 #
#la fonction 'maitre' (celle qui appelle les autres) est la fonction P_wrap, la fonction 'percoler' execute l'algorithme de Newmann-Ziff pour estimer                #
# la taille de la composante connexe plus grande d'une grille de longueur L. La fonction percoler_wrap_opt estime la probabilité d'envelopper la grille              #
######################################################################################################################################################################

def chercher(i):
    if ptr[i] < 0 :
        return i
    else:
        ptr[i] = chercher(ptr[i])
    return ptr[i]

def permutation():
    ordre = np.arange(N)
    for i in range(0,N):
        j= rd.randint(0,N-1)
        temp = ordre[i]
        ordre[i] = ordre[j]
        ordre[j] = temp 
    return(ordre)

def div_eucl(n,q):
    return(n//q,n%q)

def chercher_bis(i):
    if ptr[i] < 0 :
        return([i,[0,0]])
    else:
        r = chercher_bis(ptr[i])
        ptr[i] = r[0]
        xdep[i]=xdep[i]+r[1][0]
        ydep[i]=ydep[i]+r[1][1]
    return([ptr[i],[xdep[i],ydep[i]]])

def percoler(): #ceci est l'algorithme de Newmann-Ziff basique
    big = 1
    res = np.zeros([N]).astype('int')
    for i  in range(0,N):
        r1 = ordre[i]
        s1 = r1
        ptr[s1] = -1
        for j in range(0,K):
            s2 = voisins[s1,j]#si j=0 on va à droite, j=1 gauche; j=2 haut;j=3 bas
            print(j)
            if(ptr[s2]!=None and s2!=j):
                r2 = chercher(s2)
                if(r2 != r1):
                    if (ptr[r2]<ptr[r1]):
                        ptr[r2]=ptr[r2]+ptr[r1]
                        ptr[r1]=r2
                        r1=r2
                    else:
                        ptr[r1] = ptr[r1]+ptr[r2]
                        ptr[r2] = r1
                    if(-ptr[r1]>big):
                        big = -ptr[r1]
        res[i]=big
    return(res)

def percoler_wrap_opt(): #ceci est l'algorithme de Newmann-Ziff pour calculer la probabilité d'envelopper la grille dans un seul sens
    res = np.zeros([N]).astype('int')
    xwrap=0
    ywrap=0
    for i  in range(0,N):
        r1 = ordre[i]
        s1 = r1
        ptr[s1] = -1
        xdep[s1]=0
        ydep[s1]=0
        for j in range(0,K):
            s2 = voisins[s1,j]#si j=0 on va à droite, j=1 gauche; j=2 haut;j=3 bas
            if(ptr[s2]!=None and s2!=j):
                temp = chercher_bis(s2)
                r2 = temp[0] #est la racine de reseau du voisin
                dep_x2 = temp[1][0] #ce sont les deplacements de s2 vers sa racine
                dep_y2 = temp[1][1]
                if(r2 != r1):
                    [x,y] = direction(j,s1)#c'est le deplacement entre s1 et s2
                    temp = chercher_bis(s1)
                    dep_x1 = temp[1][0] #ce sont les deplacements de s1 vers sa racine
                    dep_y1 = temp[1][1]
                    if (ptr[r2]<ptr[r1]): #le reseau voisin est plus grand, on fusionne le reseau de r1 dans r2
                        ptr[r2]=ptr[r2]+ptr[r1]
                        ptr[r1]=r2
                        xdep[r1]= x+dep_x2-dep_x1
                        ydep[r1]=y+dep_y2-dep_y1
                        r1=r2
                    else: #le reseau voisin est plus petit, on le fusionne dans celui de r1
                        ptr[r1] = ptr[r1]+ptr[r2]
                        ptr[r2] = r1
                        xdep[r2]= -x-dep_x2+dep_x1
                        ydep[r2]= -y - dep_y2+dep_y1
                else:
                    temp = chercher_bis(s1)
                    dep_x1 = temp[1][0] #ce sont les deplacements de s1 vers sa racine
                    dep_y1 = temp[1][1]
                    #dep_x1 = xdep[s1] #ce sont les deplacements de s1
                    #dep_y1 = ydep[s1]
                    if(abs(dep_x1-dep_x2)!=0 and abs(dep_x1-dep_x2)!=1):
                        xwrap=1
                    if(abs(dep_y1-dep_y2)!=0 and abs(dep_y1-dep_y2)!=1):
                        ywrap=1
                    if(xwrap*ywrap):
                        res[i:N] = 0
                        return(res)
        if(xwrap or ywrap):
            res[i]=1
    print("erreur avec la fonction percolation")


#cette fonction appelle les autres fonctions avec les paramêtres spécifiées, pour estimer la probabilité d'envelopper ou pas la grille avec L*L arêtes la grille en question est specifié par 'choix', p_max ne change rien (pas implementé)
def P_wrap(L_glob,p_max,choix): 
    global ptr,N,xdep,ydep,voisins,K,ordre,L,direction
    L = L_glob
    N = L*L
    ptr = [None] * N
    ordre = permutation()
    if(choix == "carre"):
        V = frontiere(L)
        direction = lambda j,i: direction_carre(j,i)
    elif (choix =="carre_match"):
        V = frontiere_carre_match(L)
        direction = lambda j,i: direction_carre_match(j,i)
    elif (choix =="carre_covering"):
        V = frontiere_carre_cov_per(L)
        direction = lambda j,i: direction_carre_cov_per(j,i)
    elif(choix == "triangle"):
        V = frontiere_triangle_per(L)
        direction = lambda  j,i: direction_triangle(j,i)
    elif(choix == "hexagone"):
        V = frontiere_hexagone_per(L)
        direction = lambda  j,i:direction_hexagone(j,i,L)
    voisins = V[0]
    K = V[1]
    xdep = [None] * N
    ydep = [None] * N
    V = percoler_wrap_opt()
    return([V,ptr,ordre])



















