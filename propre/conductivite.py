import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import copy
import time
import random as rd
from mpl_toolkits import mplot3d

#from itertools import count
#import scipy.integrate as sp

M = 5
#N = 30
L = 5

nb_rep = 10

pmin = 0.0
pmax = 0.6
nb_p = 5
p = np.linspace(pmin,pmax,nb_p)

Nmin = M
Nmin = 2
Nmax = nb_p*M
Nmax = 50
nb_N = nb_p
N = np.linspace(Nmin,Nmax,nb_N,dtype=int)

display = False
dim3 = True

data = np.zeros(shape=[nb_p,nb_N])
data2 = np.zeros(shape=[nb_p,nb_N])


# on supprime effectue une percolation par arrête de la grille
def percoler(G,pp) :   
    
    H = copy.deepcopy(G)
    tabE = list(H.edges)
    nedge = len(tabE)   
    for i in range(nedge) :
        if(not rd.random() <= pp) :
            H.remove_edge(tabE[i][0],tabE[i][1])    
    return H

# donne la position linéaire dans le graphe
def getlinearPosition(GG,x,nodeOrEdge) :
    if(nodeOrEdge) : listNOE = list(GG.nodes)
    else : listNOE = list(GG.edges)
    
    nb = len(listNOE)
    for i in range(nb) : 
        if(nodeOrEdge and listNOE[i] == x) : return(i)
        elif(not(nodeOrEdge) and (listNOE[i] == x or listNOE[i] == tuple(reversed(x)))) :
            return(i)
    print("Error : object does not exist in this graph")
        
# change la couleur des arrêtes ou sommets
def change_color(GG,color_map,color,listNOE,NOE) :
    n = len(listNOE)
    for i in range(n) : 
        color_map[getlinearPosition(GG,listNOE[i],NOE)] = color
        
# renvois la liste d'arrêtes d'une suite de sommets
def convert_edge(list_node) : 
    res = []
    n = len(list_node)
    for i in range(n-1) : res.append((list_node[i],list_node[i+1]))
    
    return res

# renvois les bornes du graphes
def borne(M,N,source,L=0) : 
    res = []
    for i in range(M) : 
        if(L==1) : 
            if(source) : res.append((i,0))
            else : res.append((i,N-1))
        else :
            for j in range(L) : 
                if(source) : res.append((i,0,j))
                else : res.append((i,N-1,j))
    return res

beg = time.time()

for d in range(len(N)) :
    if(dim3) : G = nx.grid_graph(dim=(M,N[d],L))
    else : 
        G = nx.grid_2d_graph(M,N[d])
        L = 1
    if(display): pos = nx.spectral_layout(G)
    
    borneinf = borne(M=M,N=N[d],L=L,source=True)
    bornesup = borne(M=M,N=N[d],L=L,source=False)
    
    for l in range(len(p)) :
        for k in range(nb_rep) : 
            H = percoler(G,p[l])
            #data2[l,d] += H.number_of_edges()/G.number_of_edges()
            if(display):
                nb_nodes = H.number_of_nodes()
                nb_edges = H.number_of_edges()
                color_node = ['grey' for i in range(nb_nodes)]
                color_edge = ['grey' for i in range(nb_edges)]
                nx.draw(H,pos=pos,node_size=10,node_color=color_node,edge_color=color_edge)
                plt.title("p = " +str(p[l]))
                plt.show()
            
            i,j = 0,0
            while i < len(borneinf) :
                #data3[l,d] += (G.number_of_edges()-H.number_of_edges())/G.number_of_edges()
                data2[l,d] += H.number_of_edges()/G.number_of_edges()
                if(nx.has_path(H,borneinf[i],bornesup[j])) :
                    data[l,d] += 1
                    #print("inf,sup : "+str(borneinf[i])+", "+str(bornesup[j])+" p = "+str(p[l]))
                        
                    if(not display) : break        
                    else :
                        chemN = nx.shortest_path(H,borneinf[i],bornesup[j])
                        chemE = convert_edge(chemN)
                        change_color(H,color_node,'green',chemN,NOE=True)
                        change_color(H,color_edge,'green',chemE,NOE=False)
                        change_color(H,color_node,'red',borneinf,NOE=True)
                        change_color(H,color_node,'red',bornesup,NOE=True)
                        nx.draw(H,pos=pos,node_size=10,node_color=color_node,edge_color=color_edge,width=0.5)
                        plt.title("p = " +str(p[l]))
                        plt.show()
                       
                j += 1
                if(j > i) :
                    j = 0
                    i += 1
                        
    print(d)

data = data/nb_rep
data2 = data2/nb_rep
data2 = data2/np.max(data2)
est = np.unravel_index(data2.argmax(),data2.shape)


Tps = time.time() - beg
print("Execution time : ",Tps)
print(p[est[0]])

plt.contourf(p,N,np.transpose(data),levels=16)
#plt.axvline(x=0.2488,color='red')
#plt.axvline(x=0.591,color='red')
plt.axvline(x=p[est[0]],color='red')
plt.colorbar()
plt.xlabel("p")
plt.ylabel("N")
plt.show()

plt.contourf(p,N,np.transpose(data2),levels=16)
#plt.axvline(x=0.2488,color='red')
plt.axvline(x=p[est[0]],color='red')
plt.colorbar()
plt.xlabel("p")
plt.ylabel("N")
plt.show()







