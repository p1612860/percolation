import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
#import copy
import time
import random as rd
#from itertools import count
import scipy.integrate as sp

#S = safe
#I = infected
#R = recovered
#D = dead

Tf = 1000
M = 80
N = 80
taux_infect = 0.01
nb_init_infected = round(M*N*taux_infect)
#nb_init_infected = 1
time_recovery = 5
#pc = 0.5
display = False
periodic = [True, True]
repeat = True
gamma = 1/time_recovery
#theta = 1.6
beta = 2
#pI = 0.6
#pI2 = 0.3
pI2 = gamma/(beta+gamma)
#pI = beta/(beta+lbda)
#pR = 0.5
#beta = pI*gamma/(1-pI)
beta = gamma*(1-pI2)/pI2
#pR = lbda/(beta+lbda)
letality = 0.01
R0 = beta/gamma

METHODE = 2


def percoler(G,pp) :    
    tabE = list(G.edges)
    nedge = G.number_of_edges()   
    for i in range(nedge) :
        if(not rd.random() < pp) :
            G.remove_edge(tabE[i][0],tabE[i][1])           
    return G

# donne la position sur Z du noeud
def getlinearPosition(G,node) :
    listnodes = list(G.nodes)
    nb_n = len(listnodes)
    for i in range(nb_n) : 
        if(listnodes[i] == node) : return(i)
    
# initialise les infectés sur le graphe
def initInfected(G,nb_infected,mapping,bool_return=False) :
    listnodes = list(G.nodes)
    infectID = rd.sample(range(len(listnodes)),nb_infected)
    res = []
    for i in range(nb_infected) :
        res.append(listnodes[infectID[i]])
        changeState(G,listnodes[infectID[i]],'I',mapping)       
    if(bool_return) : return(res)

# modifie l'état d'un noeud 
def changeState(G,node,state,mapping) :
    position = getlinearPosition(G,node)
    G.nodes[node]['state'] = state
    if(state == 'I') : 
        mapping[position] = 'red'
        G.nodes[node]['timer'] = time_recovery
    elif(state == 'R') : mapping[position] = 'green'
    elif(state == 'S') : mapping[position] = 'blue'
    elif(state == 'D') : mapping[position] = 'black'
    else : print("Error1 : state does not exist")

# compte le nombre d'état dans le graphe
def countState(G,states) : 
    res = [0 for i in range(len(states))]
    listnodes = list(G.nodes)
    nb_n = len(listnodes)   
    for i in range(nb_n) : 
        tmp = G.nodes[listnodes[i]]
        if(tmp['state'] == states[0]) : res[0] += 1
        elif(tmp['state'] == states[1]) : res[1] += 1 
        elif(tmp['state'] == states[2]) : res[2] += 1
        elif(tmp['state'] == states[3]) : res[3] += 1 
        else : print("Error2 : state does not exist")       
    return(res)

# renvoie la liste des noeuds infectés
def getInfected(G) :
    listnodes = list(G.nodes)
    res = []
    nb_n = len(listnodes)  
    for i in range(nb_n) :
        if(G.nodes[listnodes[i]]['state'] == 'I') : 
            res.append(listnodes[i])
    return(res)

# mise a jour des infectés
def updateInfected2(G,listInfected,mapping) :
    nb_inf = len(listInfected)     
    for i in range(nb_inf) :
        if(rd.random() < letality) : 
            changeState(G,listInfected[i],'D',mapping)
        else :
            tmp = G.nodes[listInfected[i]]
            tmp['timer'] -= 1
            if (tmp['timer'] <= 0) : 
                changeState(G,listInfected[i],'R',mapping)
                
def updateInfected(G,listInfected,mapping) :
    nb_inf = len(listInfected)     
    for i in range(nb_inf) :
        if(rd.random() < letality) : 
            changeState(G,listInfected[i],'D',mapping)
        elif(rd.random() < gamma) :
            changeState(G,listInfected[i],'R',mapping)

# renvois les noeuds Sains voisins des noeuds infectés
def getSafeAjd(G,listInfected) :
      nb_inf = len(listInfected)
      res = []
      for i in range(nb_inf):
          tmp = list(G.adj[listInfected[i]])
          K = len(tmp)
          for j in range(K) :
              if(G.nodes[tmp[j]]['state']=='S') : res.append(tmp[j])   
      return(res)
  
# passage de l'état Sain a infecté
def mutateSafe(G,listSafe,mapping) :
    
    if(not repeat) : listSafe = list(set(listSafe))
    nb_s = len(listSafe)   
    for i in range(nb_s) : 
        if(G.nodes[listSafe[i]]['state']=='S' and rd.random() < pI) : 
            changeState(G,listSafe[i],'I',mapping)
            
            
def update(G,listInfected,mapping) :
    nb_inf = len(listInfected)
    for i in range(nb_inf) : 
        if(rd.random() < letality) : changeState(G,listInfected[i],'D',mapping)
        else :
            if(rd.random() < pI2) :
                changeState(G,listInfected[i],'R',mapping)
            else :
                tmp = list(G.adj[listInfected[i]])      
                k = rd.randint(0,len(tmp)-1)
                if(G.nodes[tmp[k]]['state']=='S') : 
                        changeState(G,tmp[k],'I',mapping)
            

data = np.zeros(shape=[Tf,4])
G = nx.grid_2d_graph(M,N,periodic=periodic)
#G = nx.grid_graph([M,N,N],periodic=periodic)
#G = nx.triangular_lattice_graph(M,N)
#G = nx.hexagonal_lattice_graph(M,N)
nb_nodes = nx.number_of_nodes(G)

color_map = ['blue' for i in range(nb_nodes)]

nx.set_node_attributes(G,'S','state')
#nx.get_node_attributes(G,'state')
pos = nx.spectral_layout(G)
initInfected(G,nb_init_infected,color_map)

beg = time.time()

for i in range(Tf) :
    
    tmp = countState(G,['S','I','R','D'])
    data [i,:] = tmp
    print(str(tmp)+" Iteration : "+str(i))
    
    if(display) :
        nx.draw(G,pos,node_size=10,node_color = color_map,with_labels=False)
        tmp_title = "t= "+str(i)+" # S: "+str(tmp[0])+" I: "+str(tmp[1])+" R: "+str(tmp[2])," M: "+str(tmp[3])
        plt.title(tmp_title)
        plt.show()
    
    L = getInfected(G)
    
    if(METHODE==1) :
        updateInfected(G,L,color_map)
        SF = getSafeAjd(G,L)
        mutateSafe(G,SF, color_map)
    elif(METHODE==2) :
        update(G,L,color_map)
        
    if(i>=time_recovery and np.array_equal(data[i,:],data[i-time_recovery,:])) : 
        print("Balanced i = ",i)
        T = i
        break
    
Tps = time.time() - beg
print("Execution time : ",Tps)

t = np.arange(0,T)
data = data/nb_nodes
#plt.plot(t,data[:,0],'b',t,data[:,1],'r',t,data[:,2],'g',t,data[:,],'black')
#plt.plot(t,data[t,0],'b',t,data[t,1],'r',t,data[t,2]+data[t,3],'g')
plt.plot(t,data[t,0],'b',label="S")
plt.plot(t,data[t,1],'r',label="I")
plt.plot(t,data[t,2]+data[t,3],'g',label="R")
plt.legend()
plt.xlabel("Time")
plt.ylabel("% Population")
#plt.xlim(0,T)
plt.legend()
plt.title("Marche aléatoire anisotrope")
plt.grid(True)
plt.show()
print("Max Perco:",np.argmax(data[t,1]))

def vectorfield (w,tt,param) :
    
   S, I, R = w 
   beta, lbd = param 
    
   F = [-beta*S*I,
      beta*S*I-lbd*I,
      lbd*I]
   
   return F

abserr = 1.0e-8
relerr = 1.0e-6
#stoptime = float(T)
#numpoints = T*2
#tt = [stoptime * float(i) / (numpoints - 1) for i in range(numpoints)]

param = [beta,gamma]
#param = [1.0,5.0]
w0 = [(nb_nodes-nb_init_infected)/nb_nodes,(nb_init_infected)/nb_nodes,0]
wsol = sp.odeint(vectorfield, w0, t, args=(param,),
                 atol=abserr,rtol=relerr)

plt.plot(t,wsol[:,0],'b',label="S")
plt.plot(t,wsol[:,1],'r',label="I")
plt.plot(t,wsol[:,2],'g',label="R")
plt.xlabel("Time")
plt.ylabel("% Population")
plt.legend()
plt.title("Runge-Kutta 4")
#plt.xlim(0,T)
plt.grid(True)

plt.show()
print("Max :",np.argmax(wsol[t,1]))
H1 = np.greater(np.diff(wsol[:,1]),np.diff(wsol[:,2]))
H2 = np.greater(np.diff(data[t,1]),np.diff(data[t,2]+data[t,3]))





