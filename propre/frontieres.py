# -*- coding: utf-8 -*-
"""
Created on Wed Apr 21 12:19:24 2021

@author: simon
"""

#Ce fichier contient les differentes frontieres (ce sont les aretes) et deplacements des graphes avec lesquels on a travaillé
#Il est central à l'execution des algorithmes des autres fichiers. 

import numpy as np

def frontiere(L):#c'est le carré (L^2) avec bord periodiques
    N=L*L
    K=4
    fr = np.zeros([N,4]).astype('int')
    for i in range(0,N):
        fr[i,0] = (i + 1)%N #droite
        fr[i,1] = (i - 1)%N #gauche
        fr[i,2] = (i - L)%N #haut
        fr[i,3] = (i + L)%N #bas
        if(i%L == 0):#bord gauche
            fr[i,1] = i + L -1 
        if((i+1)%L ==0):#bord droit
            fr[i,0] = i - L +1 
        if(i >= (L*(L-1))):#bord d'en bas
            fr[i,3] = i + L - L*L
        if (i<L): #bord d'en haut
            fr[i,2]= i + L*(L-1)
    return(fr,K)

def direction_carre(j,i):#cette fonction devrait changer en fonction de la grille
    x=0
    y=0
    if(j==0):
        x=1
    elif(j==1):
        x=-1
    elif(j==2):
        y=1
    elif(j==3):
        y = -1
    return([x,y])

def direction_sir(j):#on pointe vers le site qui a infecté
    x=0
    y=0
    if(j==0):
        x=-1
    elif(j==1):
        x=1
    elif(j==2):
        y=-1
    elif(j==3):
        y = 1
    return([x,y])

def frontiere_v2(L): #C'est le carré sans bords périodiques
    N=L*L
    K=4
    fr = np.zeros([N,4]).astype('int')
    for i in range(0,N):
        fr[i,0] = (i + 1)%N #droite
        fr[i,1] = (i - 1)%N #gauche
        fr[i,2] = (i - L)%N #haut
        fr[i,3] = (i + L)%N #bas
        if(i%L == 0):#bord gauche
            fr[i,1] = i 
        if((i+1)%L ==0):#bord droit
            fr[i,0] = i
        if(i >= (L*(L-1))):#bord d'en bas
            fr[i,3] = i
        if (i<L): #bord d'en haut
            fr[i,2]= i
    return(fr,K)

def frontiere_triangle(L):# C'est le reseau triangulaire sans bords pérdiodiques  
    K=6
    N=L*L
    fr = np.zeros([N,6]).astype('int')
    for i in range(0,N):
        fr[i,0] = (i + 1)%N #droite
        fr[i,1] = (i - 1)%N #gauche
        fr[i,2] = (i - L)%N #haut
        fr[i,3] = (i + L)%N #bas
        fr[i,4] = (i - 1 - L)%N #haut à gauche
        fr[i,5] = (i + 1 + L)%N #bas à droite
        if(i%L == 0):#bord gauche
            fr[i,1] = i
            fr[i,4] = i
        if((i+1)%L ==0):#bord droit
            fr[i,0] = i 
            fr[i,5] = i
        if(i >= (L*(L-1))):#bord d'en bas
            fr[i,3] = i
            fr[i,5] = i
        if (i<L): #bord d'en haut
            fr[i,2]= i 
            fr[i,4] = i
    return(fr,K)

def frontiere_hexagone(L): #C'est l'hexagone sans bords périodiques
    N= L*L
    K=3
    voisins = np.zeros((N,3)).astype('int')
    truc = [L,-L,-L,L]
    chose = [1,-1,1,-1]
    for i in range(N):
        voisins[i,0]=i+1 #droite
        voisins[i,1] = i-1 #gauche
        voisins[i,2] = i +truc[(i%L)%4]+chose[(i%L)%4] #diagonale
        if(i%L == 0 ):
            voisins[i,1]=i
        if((i+1)%L == 0):
            voisins[i,0]=i
            if(chose[(i%L)%4] == 1):
                voisins[i,2]=i
        if(voisins[i,2]<0 or voisins[i,2]>N):
            voisins[i,2]=i
    return(voisins,K)

def frontiere_carre_match(L):#c'est le graphe correspondant du graphe carré bord periodiques
    N=L*L
    K=8 
    fr = np.zeros([N,K]).astype('int')
    for i in range(0,N):
        fr[i,0] = (i + 1)%N #droite
        fr[i,1] = (i - 1)%N #gauche
        fr[i,2] = (i - L)%N #haut
        fr[i,3] = (i + L)%N #bas
        if(i%L == 0):#bord gauche
            fr[i,1] = i + L -1 
        if((i+1)%L ==0):#bord droit
            fr[i,0] = i - L +1 
        if(i >= (L*(L-1))):#bord d'en bas
            fr[i,3] = i + L - L*L
        if (i<L): #bord d'en haut
            fr[i,2]= i + L*(L-1)
    fr[:,4] = fr[fr[:,2],0] #haut droite
    fr[:,5] = fr[fr[:,3],0] #bas droite
    fr[:,6] = fr[fr[:,3],1] #bas gauche
    fr[:,7] = fr[fr[:,2],1] #haut gauche
    return(fr,K)

def direction_carre_match(j,i):
    x=0
    y=0
    if(j==0):
        x=1
    elif(j==1):
        x=-1
    elif(j==2):
        y=1
    elif(j==3):
        y = -1
    elif(j==4):
        x=1
        y=1
    elif(j==5):
        x=1
        y=-1
    elif(j==6):
        x=-1
        y=-1
    elif(j==7):
        x=-1
        y=1
    return([x,y])

def direction_carre_cov_per(j,i):
    x=0
    y=0
    pair = (i%2 ==0)
    impair = 1-pair
    if(j==0):
        x=1
    elif(j==1):
        x=-1
    elif(j==2):
        y=1
    elif(j==3):
        y = -1
    elif(j==4):
        x= pair - impair
        y=-1
    elif(j==5):
        x= impair -pair
        y=1
    return([x,y])

def frontiere_carre_cov_per(L):#c'est le graphe correspondant du graphe carré bord periodiques
    N=L*L
    K=6
    fr = np.zeros([N,K]).astype('int')
    for i in range(0,N):
        fr[i,0] = (i + 1)%N #droite
        fr[i,1] = (i - 1)%N #gauche
        fr[i,2] = (i - L)%N #haut
        fr[i,3] = (i + L)%N #bas
        if(i%L == 0):#bord gauche
            #fr[i,1]=i
            fr[i,1] = i + L -1 
            #fr[i,7] = (i -1)%N
            #fr[i,6] = (i +2*L-1)%N
        if((i+1)%L ==0):#bord droit
            #fr[i,0]=i
            fr[i,0] = i - L +1 
        if(i >= (L*(L-1))):#bord d'en bas
            #fr[i,3]=i    
            fr[i,3] = i + L - L*L
        if (i<L): #bord d'en haut
            #fr[i,2]=i
            fr[i,2]= i + L*(L-1)
    for i in range(N):
        fr[i,4] = fr[fr[i,i%2],3] #bas droite ou gauche
        fr[i,5] = fr[fr[i,(1+i)%2],2] #haut gauche ou droite
    return(fr,K)

def direction_carre_covering(j,i):#cette fonction devrait changer en fonction de la grille
    x=0
    y=0
    if(j==0):
        x=1
    elif(j==1):
        x=-1
    elif(j==2):
        y=1
    elif(j==3):
        y = -1
    elif(j==4):
        x=1
        y=-1
    elif(j==5):
        x=-1
        y=1
    return([x,y])

def direction_triangle(j,i):
    x=0
    y=0
    if(j==0):
        x=1
    elif(j==1):
        x=-1
    elif(j==2):
        y=1
    elif(j==3):
        y = -1
    elif(j==4):
        x=-1
        y=1
    elif(j==5):
        x=1
        y=-1
    return([x,y])
        
        
def frontiere_triangle_per(L):#C'est le reseau triangulaire avec bords périodiques
    K = 6
    N = L*L
    fr = np.zeros([N,6]).astype('int')
    for i in range(0,N):
        fr[i,0] = (i + 1)%N #droite
        fr[i,1] = (i - 1)%N #gauche
        fr[i,2] = (i - L)%N #haut
        fr[i,3] = (i + L)%N #bas
        fr[i,4] = (i - 1 - L)%N #haut à gauche
        fr[i,5] = (i + 1 + L)%N #bas à droite
        if(i%L == 0):#bord gauche
            fr[i,1] = i+L-1
            fr[i,4] = (i-1)%N
        if((i+1)%L ==0):#bord droit
            fr[i,0] = i -L+1
            fr[i,5] = (i+1)%N
        if(i >= (L*(L-1))):#bord d'en bas
            fr[i,3] = (i+L)%N
            fr[i,5] = (i+L+1)%N
        if (i<L): #bord d'en haut
            fr[i,2]= (i -L)%N
            fr[i,4] = (i-L-1)%N
    fr[0,4] = N-1
    fr[N-1,5]=0
    return(fr,K)

def frontiere_hexagone_per(L): #ºATTENTION L DOIT ETRE MULTIPLE DE 4!!!!!!(sinon il n'y a pas de grille perdiodique et l'algo plante)
    K = 3
    N   = L*L
    voisins = np.zeros((N,3)).astype('int')
    truc = [L,-L,-L,L]
    chose = [1,-1,1,-1]

    for i in range(N):
        voisins[i,0]= (i+1)%N #droite
        voisins[i,1] = (i-1)%N #gauche
        voisins[i,2] = (i +truc[i%4]+chose[i%4])%N#diagonale
        if(i%L == 0 ):#bord gauche
            voisins[i,1]=i+L-1
        if((i+1)%L == 0):#bord droit
            voisins[i,0]=i-L+1
    return(voisins,K)

def direction_hexagone(j,i,L=0):
    d = (i%L)%4
    x=0
    y=0
    if(j==0):
        x=1
    if(j==1):
        x=-1
    if(j==2):
        if(d==0):
            x=1
            y=-1
        elif(d==1):
            x=-1
            y=1
        elif(d==2):
            x=1
            y=1
        elif(d==3):
            x=-1
            y=-1
    return([x,y])

